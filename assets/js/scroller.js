window.onload = function() {
    document.getElementsByClassName("btn-scroll")[0].onclick = function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
};

window.onscroll = function() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        document.getElementsByClassName("btn-scroll")[0].style.display = "block";
    } else {
        document.getElementsByClassName("btn-scroll")[0].style.display = "none";
    }
};
