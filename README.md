# Projects Hub

![License badge](https://raster.shields.io/badge/license-Do%20What%20The%20F*ck%20You%20Want%20To%20Public%20License%2C%20Version%202-lightgrey.png "License badge")

![Alt text](thumbnail.png?raw=true "Thumbnail")

* [GitLab pages](https://ethicist.gitlab.io/projects-hub/)
* [Changelog](https://ethicist.gitlab.io/projects-hub/changelog/)

## License

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the [LICENSE.md](LICENSE.md) file for more details.
